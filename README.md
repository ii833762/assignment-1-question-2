# Introduction to Numerical Modelling
# Assignment 1
# Question 2

import math
import numpy as np

print ()
# Question 2 a
print ("Question 2a: Integrate any function of one variable between any \
limits with any resolution using one point Gaussian quadrature.")
print ()

def gaussquad(f, a, b, N):
    """Integrate function f using Gaussian quadrature, between limits a and \
        b, with resolution N."""
    # Width of each rectangle
    dx = (b-a)/N
    # Height of each rectangle is f(a + (i+0.5)*dx)
    # Cumulatively sum the height of each rectangle
    height = 0
    for i in range(0,N):
        height += f(a + (i+0.5)*dx)
    # Multiply by dx for total area
    area = height*dx
    return area

# Define function to be integrated
def f(x):
    function = np.sin(x)
    return function

# Set limits and resolution and print answer
print ("Integral of sin(x) between 0 and pi by Gaussian quadrature using \
20 intervals")
print ("=", gaussquad(f, 0, np.pi, 20))

print ()
# Question 2 b
print ("Question 2b: Integrate exp(x) between 0 and 1 using different numbers \
of intervals and compare with the analytical solution.")
print ()

# Define function to be integrated
def f(x):
    function = math.exp(x)
    return function

# Find the error in the Gaussian quadrature
def gaussquaderror(f, a, b, N):
    """Calculate difference between the Gaussian quadrature approximation of \
        function f and the exact integral of f"""
    exactsoln = math.e - 1
    gaussquaderror = gaussquad(f, a, b, N) - exactsoln
    return gaussquaderror

# Set limits and resolution then print answer and error
print ("Integral of exp(x) between 0 and 1 by Gaussian quadrature using \
2 intervals")
print ("=", gaussquad(f, 0, 1, 2))
print ("Error =", gaussquaderror(f, 0, 1, 2))

print ("Integral of exp(x) between 0 and 1 by Gaussian quadrature using \
20 intervals")
print ("=", gaussquad(f, 0, 1, 20))
print ("Error =", gaussquaderror(f, 0, 1, 20))

print ("Integral of exp(x) between 0 and 1 by Gaussian quadrature using \
200 intervals")
print ("=", gaussquad(f, 0, 1, 200))
print ("Error =", gaussquaderror(f, 0, 1, 200))

print ("Integral of exp(x) between 0 and 1 by Gaussian quadrature using \
2000 intervals")
print ("=", gaussquad(f, 0, 1, 2000))
print ("Error =", gaussquaderror(f, 0, 1, 2000))

